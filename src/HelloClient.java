

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class HelloClient {
	public static void main(String[] args) {
		try {
			HelloWorld obj = (HelloWorld) Naming.lookup("//" + args[0] + "/HelloWorld");
			System.out.println("Mensagem do servidor: " + obj.hello());
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}
}
