

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class HelloServer extends UnicastRemoteObject implements HelloWorld{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected HelloServer() throws RemoteException {
		super();
	}
	
	public static void main(String[] args) {
		
		HelloServer obj;
		try {
			if(args[0]!= null || args[0]!= "")
				System.setProperty("java.rmi.server.hostname", args[0]);
			System.out.println("Iniciando server em: " + args[0]);
			obj = new HelloServer();
			Naming.rebind("//localhost/HelloWorld", obj);
		} catch (RemoteException | MalformedURLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String hello() throws RemoteException {
		return "Hola, cliente!!!";
	}

}
